﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCoins : MonoBehaviour
{



    public Transform[] coinSpawns;
    public GameObject[] prefabs;

    public float chance = .5f;
    // Start is called before the first frame update
    void Start()
    {
        Spawn();
        
    }

    // Update is called once per frame
    void Spawn()
    {
        for (int i=0; i < coinSpawns.Length; i++)
        {
            //int coinFlip = Random.Range (0,2);
            if ( Random.value< chance) {
                GameObject use = prefabs[Random.Range(0,prefabs.Length)];
                Instantiate(use, coinSpawns[i].position, Quaternion.identity);
            }
                


        }
        
    }
    /*  private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player") {
            //ScoreScript.scoreValue += 10;
            Destroy (gameObject);
        }
    } */
}
